// Define schedule
new Vue({
  el: "#schedule",
  data() {
    return {
      schedule: [
        {
          day: "Monday",
          subjects: [
            {
              time: "8:00 - 8:15",
              subject: "Основи на HTML",
            },
            {
              time: "9:00 - 9:15",
              subject: "Основи на CSS",
            },
            {
              time: "12:00 - 13:15",
              subject: "Основи на JS",
            },
          ],
        },
        {
          day: "Tuesday",
          subjects: [
            {
              time: "8:00 - 8:15",
              subject: "Основи на HTML",
            },
            {
              time: "9:00 - 9:15",
              subject: "Основи на CSS",
            },
            {
              time: "10:00 - 10:15",
              subject: "Основи на JS",
            },
          ],
        },
        {
          day: "Wednesday",
          subjects: [
            {
              time: "8:00 - 8:15",
              subject: "Основи на HTML",
            },
            {
              time: "9:00 - 9:15",
              subject: "Основи на CSS",
            },
            {
              time: "10:00 - 10:15",
              subject: "Основи на JS",
            },
          ],
        },
        {
          day: "Thursday",
          subjects: [
            {
              time: "8:00 - 8:15",
              subject: "Основи на HTML",
            },
            {
              time: "9:00 - 9:15",
              subject: "Основи на CSS",
            },
            {
              time: "10:00 - 10:15",
              subject: "Основи на JS",
            },
          ],
        },
        {
          day: "Friday",
          subjects: [
            {
              time: "8:00 - 8:15",
              subject: "Основи на HTML",
            },
            {
              time: "9:00 - 9:15",
              subject: "Основи на CSS",
            },
            {
              time: "10:00 - 10:15",
              subject: "Основи на JS",
            },
          ],
        },
      ],
    };
  },
  created: function () {
    fetch("./schedule.json")
      .then((response) => response.json())
      .then((data) => {
        this.schedule = data.schedule;
      });
  },
});
